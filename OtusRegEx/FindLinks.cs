using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace OtusRegEx
{
    public class FindLinks
    {
        public string Site { get; private set; }
        public string Pattern { get; private set; }
        private string html = "";

        public FindLinks(string site)
        {
            Site = site;
            Pattern = @"(?<link>http[^\s'"">]+\.[^\s'"">]+)[\s'""<]";
            GetHTML();
        }

        private void GetHTML()
        {
            HttpWebRequest request = (HttpWebRequest) WebRequest.Create(Site);
            HttpWebResponse response = (HttpWebResponse) request.GetResponse();

            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream receiveStream = response.GetResponseStream();
                StreamReader readStream = null;

                if (String.IsNullOrWhiteSpace(response.CharacterSet))
                    readStream = new StreamReader(receiveStream);
                else
                    readStream = new StreamReader(receiveStream, Encoding.GetEncoding(response.CharacterSet));

                html = readStream.ReadToEnd();

                response.Close();
                readStream.Close();
            }

        }

        public List<string> GetLinks()
        {
            List<string> list = new List<string>();
            Match m;
            try
            {
                m = Regex.Match(html, Pattern);
                while (m.Success)
                {
                    list.Add(m.Groups["link"].ToString());
                    m = m.NextMatch();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            return list;
        }

    }
}