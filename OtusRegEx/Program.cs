﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace OtusRegEx
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string html = "";
           
            //string urlAddress = "https://stackoverflow.com/questions/16642196/get-html-code-from-website-in-c-sharp";
            Console.Write("Enter site:");
            string urlAddress = Console.ReadLine();
            FindLinks links = new FindLinks(urlAddress);
            Console.WriteLine($"Pattern - {links.Pattern}");
            Console.WriteLine();
            Console.WriteLine("Show finding url adress: ");
            foreach (var link in links.GetLinks())
            {
                Console.WriteLine(link);
            }
            Console.WriteLine();
           

        }
    }
}